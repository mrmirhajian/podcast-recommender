from flask import Response, request, Blueprint

api = Blueprint('API', __name__, 'ServerCore')

from Models import *
from app import mongo
from APIResponse import *


@api.route('/check-api', methods=["POST", "GET"])
def check_api():
    return Response(ApiResponse("OK", "Successful", "Done"), 200, "application/json")


@api.route('/Podcast', methods=["GET", "POST", "PUT", "DELETE"])
def podcast_operations():
    if request.method == "POST":
        body = request.json
        if mongo.db.Podcasts.find({}).count() == 0:
            body["id"] = 1
        else:
            body["id"] = list(mongo.db.Podcasts.find({}).sort([("id", -1)]).limit(1))[0]["id"] + 1

        sub_subjects = []
        for i, ss in enumerate(body["sub_subjects"]):
            sub_subjects.append(SubSubject(i + 1, ss).__dict__)

        podcast = Podcast(body["id"], body["name"], body["desc"], body["main_subject"], sub_subjects)

        res = mongo.db.Podcasts.save(podcast.__dict__)
        return Response(str(res) + " created successfully!")

    elif request.method == "PUT":
        body = request.json

        if mongo.db.Podcasts.find({"id": body["id"]}).count() < 1:
            return "This Podcast doesn't exist!"

        sub_subjects = []
        for i, ss in enumerate(body["sub_subjects"]):
            sub_subjects.append(SubSubject(i + 1, ss).__dict__)

        podcast = Podcast(body["id"], body["name"], body["desc"], body["main_subject"], sub_subjects)

        res = mongo.db.Podcasts.update({"id": body["id"]}, podcast.__dict__)
        return Response(json.dumps(res), mimetype="application/json")

    elif request.method == "GET":
        body = request.json
        res = list(mongo.db.Podcasts.find({"id": body["id"]}, {'_id': 0}))
        return Response(json.dumps(res), mimetype="application/json")

    elif request.method == "DELETE":
        return 1


@api.route('/Podcasts', methods=["POST", "GET"])
def get_podcasts():
    if request.method == "GET":
        res = list(mongo.db.Podcasts.find({}, {'_id': 0}))
        return Response(json.dumps(res), mimetype="application/json")


@api.route('/User', methods=["POST"])
def user_operations():
    user_form = request.json

    ratings = []
    for rate in user_form["ratings"]:
        sub_rates = []
        for sub_rate in rate["sub_ratings"]:
            sub_rates.append(SubRate(int(sub_rate["id"]), sub_rate["name"], int(sub_rate["rate"])).__dict__)

        ratings.append(Rate(int(rate["id"]), rate["podcast_name"], rate["main_subject_name"],
                            int(rate["main_subject_rate"]), sub_rates).__dict__)

    user = User(int(user_form["gender"]), int(user_form["age"]), ratings)

    res = mongo.db.Users.save(user.__dict__)
    return Response(str(res) + " created successfully!")


@api.route('/Users', methods=["GET"])
def get_users():
    res = list(mongo.db.Users.find({}, {'_id': 0}))
    return Response(json.dumps(res), mimetype="application/json")
